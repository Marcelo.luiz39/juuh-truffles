import "./Sidebar.css";
import Logo from "../../assets/logo.jpg";

export default function Sidebar({ sidebarOpen, closeSidebar }) {
  return (
    <div className={sidebarOpen ? "sidebar-responsive" : ""} id="sidebar">
      <div className="sidebar__title">
        <div className="sidebar__img">
          <img src={Logo} alt="logo" />
          <h1>Juuh's truffles</h1>
        </div>
        <i
          onClick={() => closeSidebar()}
          className="fa fa-times"
          id="sidebarIcon"
          aria-hidden="true"
        ></i>
      </div>

      <div className="sidebar__menu">
        <div className="sidebar__link active_menu_link">
          <i className="fa fa-minus-square"></i>
          <a href="#">Home</a>
        </div>
        <h2>Admin</h2>
        <div className="sidebar__link">
          <i className="fa fa-tachometer" aria-hidden="true"></i>
          <a href="#">Área administrativa</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-user" aria-hidden="true"></i>
          <a href="#">Usuários</a>
        </div>
        <h2>Produtos</h2>
        <div className="sidebar__link">
          <i className="fa fa-archive" aria-hidden="true"></i>
          <a href="#">Produtos</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-bars" aria-hidden="true"></i>
          <a href="#">Categoria</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-cutlery" aria-hidden="true"></i>
          <a href="#">Pedidos</a>
        </div>
        <h2>Pessoas</h2>
        <div className="sidebar__link">
          <i className="fa fa-male" aria-hidden="true"></i>
          <a href="#">Administradores</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-user-circle" aria-hidden="true"></i>
          <a href="#">Usuários</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-money" aria-hidden="true"></i>
          <a href="#">Pagamentos e custos</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-tasks" aria-hidden="true"></i>
          <a href="#">A plataforma</a>
        </div>
        <div className="sidebar__link">
          <i className="fa fa-file-text" aria-hidden="true"></i>
          <a href="#">Politica de privacidade</a>
        </div>
        <div className="sidebar__logout">
          <i className="fa fa-power-off" aria-hidden="true"></i>
          <a href="#">Logout</a>
        </div>
      </div>
    </div>
  );
}
