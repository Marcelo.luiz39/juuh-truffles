import "./Main.css";
import Hello from "../../assets/hello.svg";
import Chart from "../chart/Chart";

export default function main() {
  return (
    <main>
      <div className="main__container">
        <div className="main__title">
          <img src={Hello} alt="hello" />
          <div className="main__greeting">
            <h1>Olá Julia</h1>
            <p>Bem vindo ao painel</p>
          </div>
        </div>
        <div className="main__cards">
          <div className="card">
            <i
              className="fa fa-file-text fa-2x text-lightblue"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Número de pedidos</p>
              <span className="font-bold text-title">578</span>
            </div>
          </div>

          <div className="card">
            <i className="fa fa-money fa-2x text-red" aria-hidden="true"></i>
            <div className="card_inner">
              <p className="text-primary-p">Pagamentos</p>
              <span className="font-bold text-title">R$2.467</span>
            </div>
          </div>

          <div className="card">
            <i
              className="fa fa-archive fa-2x text-yellow"
              aria-hidden="true"
            ></i>
            <div className="card_inner">
              <p className="text-primary-p">Nome do produto</p>
              <span className="font-bold text-title">trufa de chocolate</span>
            </div>
          </div>

          <div className="card">
            <i className="fa fa-bars fa-2x text-green" aria-hidden="true"></i>
            <div className="card_inner">
              <p className="text-primary-p">Categorias</p>
              <span className="font-bold text-title">Sabores</span>
            </div>
          </div>
        </div>

        <div className="charts">
          <div className="charts__left">
            <div className="charts__left__title">
              <div>
                <h1>Gráfico do Mês</h1>
                <p>São Paulo, SP</p>
              </div>
              <i className="fa fa-usd" aria-hidden="true"></i>
            </div>
            <Chart />
          </div>

          <div className="charts__right">
            <div className="charts__right__title">
              <div>
                <h1>Relatórios diários</h1>
                <p>São Paulo, SP</p>
              </div>
              <i className="fa fa-area-chart"></i>
            </div>

            <div className="charts__right__cards">
              <div className="card1">
                <h1>Trufas vendidas</h1>
                <p>Quant: 100</p>
              </div>

              <div className="card2">
                <h1>Pagamentos</h1>
                <p>R$ 3.480</p>
              </div>

              <div className="card3">
                <h1>Custos material</h1>
                <p>R$ 480</p>
              </div>

              <div className="card4">
                <h1>Receita Total</h1>
                <p>R$ 3.000</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}
